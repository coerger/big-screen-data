import { FC, useEffect, useRef, useState } from "react";
import CountUp from "react-countup";
const UsersCount: FC<{}> = (props) => {
  let growthLastDay = useRef(12.44);
  let growthLastMonth = useRef(15.76);
  let todayUser = useRef(10000);
  let growthLastDayBefore = useRef(0);
  let growthLastMonthBefore = useRef(0);
  let todayUserBefore = useRef(0);
  const [first, setfirst] = useState(0);
  useEffect(() => {
    let task: any;
    task = setInterval(() => {
      growthLastDayBefore.current = growthLastDay.current;
      growthLastMonthBefore.current = growthLastMonth.current;
      todayUserBefore.current = todayUser.current;
      growthLastDay.current += 0.74;
      growthLastMonth.current += 0.35;
      todayUser.current += 14;
      setfirst((first) => first + 1);
    }, 2000);
    return () => {
      task && clearInterval(task);
    };
  }, []);

  return (
    <div className="UsersCount">
      <h1>用户总数</h1>
      <div className="subTitle">User Total Count</div>
      <div className="subTitle1">
        <CountUp start={todayUserBefore.current} end={todayUser.current} duration={1} />
      </div>

      <div className="subTitle green ">
        每日增长率：
        <CountUp start={growthLastDayBefore.current} end={growthLastDay.current} decimals={2} duration={1} />
        % 每月增长率： <CountUp start={growthLastMonthBefore.current} end={growthLastMonth.current} decimals={2} duration={1} />%
      </div>
      <div className="line-wrapper">
        <div className="inner" style={{ width: `${growthLastMonth.current}%` }}></div>
      </div>
    </div>
  );
};

export default UsersCount;
