import { FC, useEffect, useRef, useState } from "react";
import { NightingaleChart } from "coerger-design";
import CountUp from "react-countup";
const Device: FC<{}> = (props) => {
  let male = useRef(0);
  let famale = useRef(0);
  let ageArr = useRef([
    {
      name: "Android",
      value: 14500,
    },
    {
      name: "IOS",
      value: 15233,
    },
    {
      name: "PC",
      value: 9080,
    },
  ]);

  const [first, setfirst] = useState(0);
  useEffect(() => {
    let task: any;
    task = setInterval(() => {
      male.current += 1;
      famale.current += 2;
      for (let index = 0; index < ageArr.current.length; index++) {
        ageArr.current[index].value += 1;
      }
      setfirst((first) => first + 1);
    }, 2000);
    return () => {
      task && clearInterval(task);
    };
  }, []);

  return (
    <div className="Device">
      <div className="chart">
        <NightingaleChart width={176} height={176} data={ageArr.current} radius={[0, 90]}></NightingaleChart>
      </div>
      <div className="titleBox">
        <div className="device-title">
          <div>
            <h1>登录设备</h1>
            <div className="subTitle">Number of device</div>
          </div>
          <div className="subTitle1 sui">
            <CountUp end={famale.current} duration={1} />
            <span style={{ fontSize: "30px" }}> 万人</span>
          </div>
        </div>
        <div className="ageType">
          {ageArr.current.map((item, index) => {
            return (
              <div className="age-item" key={index}>
                <div className="subTitle">
                  <CountUp end={item.value} duration={1} />
                </div>
                <div className="cirBox">
                  <div className="cir"></div>
                  <span style={{ fontSize: "20px" }}>{item.name}</span>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Device;
