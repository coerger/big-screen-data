import { FC, useEffect, useRef, useState } from "react";
import EChartsReact from "echarts-for-react";
const HotSellingCategories: FC<{}> = (props) => {
  const ref = useRef() as any;

  const option = useRef({
    title: {
      text: "热销商品",
      subtext: "HotSellingCategories",
      left: "left",
      textStyle: {
        fontSize: "2.5rem",
        color: "white",
      },
      subtextStyle: {
        fontSize: "20px",
        color: "white",
      },
    },
    legend: {
      right: 10,
      textStyle: {
        color: "white",
      },
    },
    xAxis: {
      type: "category",
      data: ["商品1", "商品2", "商品3", "商品4", "商品5", "商品6", "商品7"],
      axisLabel: {
        color: "white",
        fontSize: "20px",
      },
    },
    yAxis: {
      type: "value",
      axisLabel: {
        color: "white",
        fontSize: "20px",
      },
    },
    series: [
      {
        data: [120, 200, 150, 80, 70, 110, 130],
        type: "bar",
      },
    ],
    grid: {
      top: 90,
      left: 50,
      right: 0,
      bottom: 35,
    },
  });
  useEffect(() => {
    let task: any;
    const echart = ref.current.getEchartsInstance();
    task = setInterval(() => {
      for (let index = 0; index < option.current.series[0].data.length; index++) {
        option.current.series[0].data[index] = option.current.series[0].data[index] + 10;
      }
      echart.setOption(option.current);
    }, 2000);
    return () => {
      task && clearInterval(task);
    };
  }, []);

  return (
    <div className="HotSellingCategories">
      <EChartsReact option={option.current} style={{ height: "100%" }} ref={ref}></EChartsReact>
    </div>
  );
};

export default HotSellingCategories;
