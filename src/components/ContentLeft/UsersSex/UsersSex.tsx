import { FC, useEffect, useRef, useState } from "react";
import CountUp from "react-countup";
const UsersSex: FC<{}> = (props) => {
  let male = useRef(0);
  let famale = useRef(0);
  const [first, setfirst] = useState(0);
  useEffect(() => {
    let task: any;
    task = setInterval(() => {
      male.current += 1;
      famale.current += 3;
      setfirst((first) => first + 1);
    }, 2000);
    return () => {
      task && clearInterval(task);
    };
  }, []);

  return (
    <div className="UsersSex">
      <div className="maleBox">
        <div className="sex-logo">
          <svg viewBox="0 0 1024 1024">
            <path d="M629.845333 333.824L835.669333 128H725.333333a42.666667 42.666667 0 1 1 0-85.333333h213.333334a42.666667 42.666667 0 0 1 42.666666 42.666666v213.333334a42.666667 42.666667 0 1 1-85.333333 0V188.330667l-205.824 205.824A361.109333 361.109333 0 0 1 768 618.666667c0 200.298667-162.368 362.666667-362.666667 362.666666S42.666667 818.965333 42.666667 618.666667s162.368-362.666667 362.666666-362.666667c84.778667 0 162.752 29.077333 224.512 77.824zM405.333333 896c153.173333 0 277.333333-124.16 277.333334-277.333333s-124.16-277.333333-277.333334-277.333334S128 465.493333 128 618.666667s124.16 277.333333 277.333333 277.333333z"></path>
          </svg>
        </div>
        <div>
          <h1>男性用户人数</h1>
          <div className="subTitle">Number of male users</div>
          <div className="subTitle1">
            <CountUp end={male.current} duration={1} />
            <span style={{ fontSize: "30px" }}> 万人</span>
          </div>
        </div>
      </div>
      <div className="maleBox">
        <div className="sex-logo">
          <svg viewBox="0 0 1024 1024" version="1.1">
            <path d="M576 568c110.4-28.4 192-128.6 192-248 0-141.4-114.6-256-256-256S256 178.6 256 320c0 119.2 81.6 219.4 192 248l0 136-128 0 0 128 128 0 0 128 128 0 0-128 128 0 0-128-128 0L576 568zM512 480c-88.2 0-160-71.8-160-160s71.8-160 160-160 160 71.8 160 160S600.2 480 512 480z"></path>
          </svg>
        </div>
        <div>
          <h1>女性用户人数</h1>
          <div className="subTitle">Number of male users</div>
          <div className="subTitle1">
            <CountUp end={famale.current} duration={1} />
            <span style={{ fontSize: "12px" }}> 万人</span>
          </div>
        </div>
      </div>
      {/* <h3>
        <CountUp end={male.current} duration={1} />
      </h3> */}
    </div>
  );
};

export default UsersSex;
