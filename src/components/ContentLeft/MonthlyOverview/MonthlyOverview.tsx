import { FC } from "react";
import EChartsReact from "echarts-for-react";
const MonthlyOverview: FC<{}> = (props) => {
  const option = {
    title: {
      text: "每日概况",
      subtext: "DailyOverview",
      left: "left",
      textStyle: {
        fontSize: "2.5rem",
        color: "white",
      },
      subtextStyle: {
        fontSize: "20px",
        color: "white",
      },
    },
    legend: {
      right: 10,
      textStyle: {
        color: "white",
      },
      data: [
        {
          name: "男",
          icon: "rect",
        },
        {
          name: "女",
          icon: "rect",
        },
      ],
    },
    xAxis: {
      type: "category",
      data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
      axisLabel: {
        color: "white",
        fontSize: "20px",
      },
    },
    yAxis: {
      type: "value",
      axisLabel: {
        color: "white",
        fontSize: "20px",
      },
    },
    series: [
      {
        name: "男",
        data: [150, 230, 224, 218, 135, 147, 260],
        type: "line",
      },
      {
        name: "女",
        data: [10, 43, 34, 24, 145, 147, 24],
        type: "line",
      },
    ],
    grid: {
      top: 90,
      left: 50,
      right: 0,
      bottom: 20,
    },
  };

  return (
    <div className="MonthlyOverview">
      <EChartsReact option={option} style={{ height: "100%" }}></EChartsReact>
    </div>
  );
};

export default MonthlyOverview;
