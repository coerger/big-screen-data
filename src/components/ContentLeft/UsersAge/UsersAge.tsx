import { FC, useEffect, useRef, useState } from "react";
import { ChartLines } from "coerger-design";
import CountUp from "react-countup";
const UsersAge: FC<{}> = (props) => {
  let todayUser = useRef(15);
  let todayUserBefore = useRef(0);

  let ageArr = useRef([
    {
      type: "0-20",
      data: 1200,
    },
    {
      type: "20-30",
      data: 1500,
    },
    {
      type: "30-40",
      data: 900,
    },
    {
      type: "≥50",
      data: 500,
    },
  ]);
  const [first, setfirst] = useState(0);

  useEffect(() => {
    let task: any;
    task = setInterval(() => {
      todayUserBefore.current = todayUser.current;
      todayUser.current += 0.88;
      for (let index = 0; index < ageArr.current.length; index++) {
        ageArr.current[index].data += 1;
      }
      setfirst((first) => first + 1);
    }, 2000);
    return () => {
      task && clearInterval(task);
    };
  }, []);
  return (
    <div className="UsersAge">
      <h1>用户年龄分布&平均年龄</h1>
      <div className="subTitle">User age distribution&average age</div>
      <div className="subTitle1 sui">
        <CountUp start={todayUserBefore.current} end={todayUser.current} duration={1} decimals={2} />
        <span style={{ fontSize: "30px" }}> 岁</span>
      </div>
      <div className="ChartLinesBox">
        <ChartLines width={800} height={50}></ChartLines>
      </div>

      <div className="ageType">
        {ageArr.current.map((item, index) => {
          return (
            <div className="age-item" key={index}>
              <div>
                <CountUp end={item.data} duration={1} />
              </div>
              <div className="cirBox">
                <div className="cir"></div>
                <span>{item.type}</span>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default UsersAge;
