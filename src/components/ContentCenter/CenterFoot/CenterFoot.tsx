import { FC, useEffect, useRef, useState } from "react";
import * as echarts from "echarts";
import EChartsReact from "echarts-for-react";
const CenterFoot: FC<{}> = (props) => {
  const ref = useRef() as any;
  const option = useRef({}) as any;
  useEffect(() => {
    let data = getVirtulData(2016);

    option.current = {
      backgroundColor: "#404a59",
      tooltip: {
        trigger: "item",
      },
      calendar: [
        {
          left: "0",
          right: 0,
          top: 0,
          bottom: 0,
          range: ["2016-01-01", "2016-12-31"],
          splitLine: {
            show: true,
            lineStyle: {
              color: "#000",
              width: 4,
              type: "solid",
            },
          },
          itemStyle: {
            normal: {
              color: "#323c48",
              borderWidth: 1,
              borderColor: "#111",
            },
          },
        },
      ],
      series: [
        {
          name: "步数",
          type: "scatter",
          coordinateSystem: "calendar",
          data: data,
          symbolSize: function (val: any) {
            return val[1] / 500;
          },
          itemStyle: {
            normal: {
              color: "#ddb926",
            },
          },
        },
        {
          name: "步数",
          type: "scatter",
          coordinateSystem: "calendar",
          calendarIndex: 1,
          data: data,
          symbolSize: function (val: any) {
            return val[1] / 500;
          },
          itemStyle: {
            normal: {
              color: "#ddb926",
            },
          },
        },
        {
          name: "Top 12",
          type: "effectScatter",
          coordinateSystem: "calendar",
          calendarIndex: 1,
          data: data
            .sort(function (a, b) {
              return (b[1] as any) - (a[1] as any);
            })
            .slice(0, 12),
          symbolSize: function (val: any) {
            return val[1] / 500;
          },
          showEffectOn: "render",
          rippleEffect: {
            brushType: "stroke",
          },
          hoverAnimation: true,
          itemStyle: {
            normal: {
              color: "#f4e925",
              shadowBlur: 10,
              shadowColor: "#333",
            },
          },
          zlevel: 1,
        },
        {
          name: "Top 12",
          type: "effectScatter",
          coordinateSystem: "calendar",
          data: data
            .sort(function (a, b) {
              return (b[1] as any) - (a[1] as any);
            })
            .slice(0, 12),
          symbolSize: function (val: any) {
            return val[1] / 500;
          },
          showEffectOn: "render",
          rippleEffect: {
            brushType: "stroke",
          },
          hoverAnimation: true,
          itemStyle: {
            normal: {
              color: "#f4e925",
              shadowBlur: 10,
              shadowColor: "#333",
            },
          },
          zlevel: 1,
        },
      ],
    };
    const echart = ref.current.getEchartsInstance();
    echart.setOption(option.current);
  }, []);

  function getVirtulData(year: any) {
    year = year || "2017";
    var date = +echarts.number.parseDate(year + "-01-01");
    var end = +echarts.number.parseDate(+year + 1 + "-01-01");
    var dayTime = 3600 * 24 * 1000;
    var data = [];
    for (var time = date; time < end; time += dayTime) {
      data.push([echarts.format.formatTime("yyyy-MM-dd", time), Math.floor(Math.random() * 10000)]);
    }
    return data;
  }
  return (
    <div className="CenterFoot">
      <EChartsReact option={option.current} style={{ height: "100%" }} ref={ref}></EChartsReact>
    </div>
  );
};

export default CenterFoot;
