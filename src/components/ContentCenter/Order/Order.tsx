import { FC, useEffect, useRef, useState } from "react";
import EChartsReact from "echarts-for-react";
import CountUp from "react-countup";
import { FlyBox } from "coerger-design";
const Order: FC<{
  children?: any;
}> = (props) => {
  let trend = useRef(0);
  let growth = useRef(0);

  const [first, setfirst] = useState(0);
  useEffect(() => {
    let task: any;
    task = setInterval(() => {
      trend.current += 100;
      growth.current += 0.88;
      setfirst((first) => first + 1);
    }, 2000);
    return () => {
      task && clearInterval(task);
    };
  }, []);
  function randomData() {
    now = new Date(+now + oneDay);
    value = value + Math.random() * 21 - 10;
    return {
      name: now.toString(),
      value: [[now.getFullYear(), now.getMonth() + 1, now.getDate()].join("/"), Math.round(value)],
    };
  }
  let data = [];
  let now = new Date(1997, 9, 3);
  let oneDay = 24 * 3600 * 1000;
  let value = Math.random() * 1000;
  for (var i = 0; i < 1000; i++) {
    data.push(randomData());
  }
  const option = {
    tooltip: {
      trigger: "axis",
      formatter: function (params: any) {
        params = params[0];
        var date = new Date(params.name);
        return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " : " + params.value[1];
      },
      axisPointer: {
        animation: false,
      },
    },
    xAxis: {
      type: "time",
      splitLine: {
        show: false,
      },
      axisLabel: {
        color: "white",
        fontSize: "20px",
      },
    },
    yAxis: {
      type: "value",
      boundaryGap: [0, "100%"],
      splitLine: {
        show: false,
      },
      axisLabel: {
        color: "white",
        fontSize: "20px",
      },
    },
    series: [
      {
        name: "Fake Data",
        type: "line",
        showSymbol: false,
        data: data,
      },
    ],
    grid: {
      top: 15,
      right: 0,
      bottom: 25,
      left: 40,
    },
  };

  return (
    <div className="Order">
      <div className="title">
        <div>
          <h1>趋势图</h1>
          <div className="subTitle">trend of charts</div>
          <div className="subTitle1">
            <CountUp end={trend.current} duration={1} />
          </div>
          <div className="subTitle">同比增长率</div>
          <div className="subTitle1">
            <CountUp end={growth.current} duration={1} decimals={2} />%
          </div>
        </div>
      </div>
      <EChartsReact option={option} style={{ width: "100%", height: "100%" }} className="chart"></EChartsReact>
      <FlyBox></FlyBox>
    </div>
  );
};

export default Order;
